import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {Observable} from 'rxjs';
import {User} from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private user$: Observable<User>;
  private user: User;

  constructor(private firebaseAuth: AngularFireAuth) {
    this.user$ = firebaseAuth.authState;
    this.user$.subscribe(
      (user) => {
        if (user) {
          this.user = user;
        } else {
          this.user = null;
        }
      }
    );
  }

  login(email: string, password: string) {
    return this.firebaseAuth.auth.signInWithEmailAndPassword(email, password);
  }

  logout() {
    return this.firebaseAuth.auth.signOut();
  }

  register(email: string, password: string) {
    this.firebaseAuth.auth.createUserWithEmailAndPassword(email, password)
      .then(
        (userCredential) => {
          console.log('Registration successful');
          console.log(`User credentials: ${userCredential}`);
        },
        (errorReason) => {
          console.log('Registration failed');
          console.log(`Error reason: ${errorReason}`);
        }
      );
  }

  isLoggedIn(): boolean {
    return !(this.user == null);
  }
}
