import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../auth.service';
import {FormBuilder, Validators} from '@angular/forms';
import {ToolbarService} from '../../toolbar.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm = this.formBuilder.group({
    email: ['', [Validators.email, Validators.required]],
    password: ['', [Validators.required]]
  });

  constructor(private authService: AuthService,
              private router: Router,
              private formBuilder: FormBuilder,
              private toolbarService: ToolbarService) {
  }

  login(email: string, password: string) {
    this.authService.login(email, password).then(
      () => {
        this.router.navigate(['/dashboard/buildings']).then(() => {
          this.toolbarService.showToolbarLinks();
        });
      },
      (reason) => {
        if (reason.code === 'auth/user-not-found') {
          this.email.setErrors({userNotFound: true});
        } else if (reason.code === 'auth/wrong-password') {
          this.password.setErrors({wrongPassword: true});
        } else {
          console.log(`unrecognized authentication error: ${reason}`);
        }
      });
  }

  onSubmit() {
    console.log(this.loginForm.value);
    this.login(this.email.value, this.password.value);
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }
}
