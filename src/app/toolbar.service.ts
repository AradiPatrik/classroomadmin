import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToolbarService {

  private onDashboard = new Subject<boolean>();
  isOnDashboard = this.onDashboard.asObservable();

  constructor() {
  }

  showToolbarLinks() {
    this.onDashboard.next(true);
  }

  hideToolbarLinks() {
    this.onDashboard.next(false);
  }
}
