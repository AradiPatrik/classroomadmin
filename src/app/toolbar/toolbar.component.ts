import {Component, OnInit} from '@angular/core';
import {ToolbarService} from '../toolbar.service';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  shouldShowNavigation: Observable<boolean>;

  constructor(private toolbarService: ToolbarService) {
  }

  ngOnInit() {
    this.shouldShowNavigation = of(true);
    this.shouldShowNavigation = this.toolbarService.isOnDashboard;
  }

}
