import {AfterViewInit, ChangeDetectorRef, Component, Input} from '@angular/core';
import {Building} from '../building';

@Component({
  selector: 'app-building-card',
  templateUrl: './building-card.component.html',
  styleUrls: ['./building-card.component.css']
})
export class BuildingCardComponent {
  @Input() building: Building;
}
