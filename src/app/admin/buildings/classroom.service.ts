import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {UniqueIdService} from '../unique-id.service';

export interface Classroom {
  id: number;
  buildingId: number;
  code: string;
  direction: string;
  name: string;
  navNodeId: number;
  position: number;
}

export interface ClassroomUpdate {
  name: string;
  code: string;
  position: string;
  direction: string;
}

function mapToClassrooms(classroomsSnapshot): Classroom[] {
  return classroomsSnapshot.map(snapshot => ({
    id: +snapshot.key,
    buildingId: +snapshot.payload.child('buildingId').val(),
    code: snapshot.payload.child('code').val(),
    direction: snapshot.payload.child('direction').val(),
    name: snapshot.payload.child('name').val(),
    navNodeId: +snapshot.payload.child('navNodeId').val(),
    position: +snapshot.payload.child('position').val()
  }));
}

@Injectable({
  providedIn: 'root'
})
export class ClassroomService {

  constructor(private db: AngularFireDatabase, private uniqueIdService: UniqueIdService) {
  }

  getClassroomsOfNavNode(navNodeId: number): Observable<Classroom[]> {
    return this.db
      .list('/classrooms', ref => ref.orderByChild('navNodeId').equalTo(navNodeId))
      .snapshotChanges().pipe(
        map(classroomsSnapshot => mapToClassrooms(classroomsSnapshot)),
        tap(classrooms => {
          console.log(classrooms);
        })
      );
  }

  updateClassroom(id: number, value: ClassroomUpdate) {
    this.db.object(`/classrooms/${id}`).update(value).then(() => {
      console.log('update successful');
    }, (reason) => {
      console.log('update was not successful');
      console.log(reason);
    });
  }

  createEmptyClassroom(buildingId: number, navNodeId) {
    this.uniqueIdService.getUniqueIdOf('/classrooms').subscribe((id) => {
      this.db.object(`/classrooms/${id}`).set({
        buildingId,
        code: 'Code not set',
        direction: 'left',
        name: 'Name not set',
        navNodeId,
        position: 0
      }).then(() => {
        console.log('create successful');
      }, (reason) => {
        console.log('create was not successful');
        console.log(reason);
      });
    });
  }

  deleteClassroom(id: number) {
    this.db.object(`/classrooms/${id}`).remove().then(() => {
      console.log('delete successful');
    }, (reason) => {
      console.log('delete was not successful');
      console.log(reason);
    });
  }
}
