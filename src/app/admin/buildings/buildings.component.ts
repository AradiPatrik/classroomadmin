import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';
import {merge, Observable, of, Subject, Subscription} from 'rxjs';
import {Building} from './building';
import {BuildingService} from './building.service';
import {catchError, startWith, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-buildings',
  templateUrl: './buildings.component.html',
  styleUrls: ['./buildings.component.css']
})
export class BuildingsComponent implements OnInit, OnDestroy{

  allBuildings$: Observable<Building[]>;
  private subscription: Subscription;
  loaded = false;

  constructor(private buildingService: BuildingService) {
    this.allBuildings$ = this.buildingService.allBuildings$;
  }

  ngOnInit(): void {
    this.subscription = this.allBuildings$.subscribe(() => this.loaded = true);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
