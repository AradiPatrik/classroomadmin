import {SnapshotAction} from '@angular/fire/database';

export interface Building {
  id: string;
  name: string;
  imageUrl: string;
  streetName: string;
}

export function buildingFromSnapshot(snapshot: SnapshotAction<any>): Building {
  const id = snapshot.key;
  const name = snapshot.payload.child('name').val();
  const imageUrl = snapshot.payload.child('imageUrl').val();
  const streetName = snapshot.payload.child('streetName').val();
  return {id, name, imageUrl, streetName};
}
