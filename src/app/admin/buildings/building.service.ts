import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {Observable} from 'rxjs';
import {Building, buildingFromSnapshot} from './building';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BuildingService {
  allBuildings$: Observable<Building[]>;

  constructor(private db: AngularFireDatabase) {
    this.allBuildings$ = this.db.list('/buildings').snapshotChanges().pipe(
      map((buildingNodes) => this.mapToBuildings(buildingNodes)),
      tap(() => console.log('buildings arrived'))
    );
  }

  private mapToBuildings(buildingNodes) {
    return buildingNodes.map(snapshot => buildingFromSnapshot(snapshot));
  }
}
