import {ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {BuildingGraphService} from '../../building-graph.service';
import {Subscription} from 'rxjs';
import {NavNodeService} from '../../nav-node.service';

@Component({
  selector: 'app-graph-display',
  templateUrl: './graph-display.component.html',
  styleUrls: ['./graph-display.component.css']
})
export class GraphDisplayComponent implements OnInit, OnDestroy {

  selectedId = '';
  @Output() nodeClick = new EventEmitter<number>();
  @Output() addChildClick = new EventEmitter<number>();
  loaded = false;
  private subscription: Subscription;

  constructor(private buildingGraphService: BuildingGraphService, private navNodeService: NavNodeService) {
  }

  ngOnInit() {
    this.subscription = this.buildingGraphService.graphReady$
      .subscribe(() => {
        this.loaded = true;
        console.log('this happened');
      });

  }

  onClick(id: string) {
    this.selectedId = id;
    this.nodeClick.emit(+id);
    console.log(this.buildingGraphService.isLeaf(+id));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  addChild(): void {
    this.addChildClick.emit(+this.selectedId);
  }

  isSelectedLeaf() {
    return this.buildingGraphService.isLeaf(+this.selectedId);
  }

  del() {
    this.buildingGraphService.deleteNode(+this.selectedId);
    this.navNodeService.deleteNode(+this.selectedId);
  }
}
