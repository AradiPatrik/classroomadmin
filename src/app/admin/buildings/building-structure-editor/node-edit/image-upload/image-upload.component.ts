import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NavNode} from '../../../nav-node.service';
import {AngularFireStorage} from '@angular/fire/storage';
import {Observable, Subject} from 'rxjs';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {

  @Input() navNode: NavNode;
  @Output() imageUploadStarted = new EventEmitter();
  @Output() imageUploadFinished = new EventEmitter();

  isHovered = false;

  isLoading = false;
  percentChanges: Observable<number | any>;

  constructor(private fireStorage: AngularFireStorage) {
  }

  ngOnInit() {
  }

  onDragOver($event: DragEvent) {
    $event.preventDefault();
    $event.stopPropagation();
    console.log($event);
    console.log('on drag over');
    this.isHovered = true;
    console.log(this.isHovered);
  }

  onDrop($event: DragEvent) {
    $event.preventDefault();
    console.log($event);
    console.log('onDrop');

    if (!$event.dataTransfer.items ||
      $event.dataTransfer.items.length !== 1 ||
      $event.dataTransfer.items[0].kind !== 'file') {
      return;
    }

    const file = $event.dataTransfer.items[0].getAsFile();
    console.log(file.name);

    this.imageUploadStarted.emit();
    const uploadTask = this.fireStorage.upload(`/nav_node_pictures/${file.name}`, file);

    this.percentChanges = uploadTask.percentageChanges();

    this.isLoading = true;
    uploadTask.then(snapshot => {
      snapshot.ref.getDownloadURL().then(value => {
        this.navNode.imageUrl = value;
        console.log(value);
        this.isLoading = false;
        this.imageUploadFinished.emit();
      });
    });
  }

  onDragExit($event: Event) {
    console.log('onDragExit');
    this.isHovered = false;
    console.log(this.isHovered);
  }
}
