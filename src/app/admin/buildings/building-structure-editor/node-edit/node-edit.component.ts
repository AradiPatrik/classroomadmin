import {Component, Input, OnInit} from '@angular/core';
import {NavNode, NavNodeService, UpdateData} from '../../nav-node.service';
import {FormBuilder, Validators} from '@angular/forms';


@Component({
  selector: 'app-node-edit',
  templateUrl: './node-edit.component.html',
  styleUrls: ['./node-edit.component.css']
})
export class NodeEditComponent implements OnInit {

  navNode: NavNode;
  isEditing = false;
  imageUploadPercentage = 0;

  types = [
    'corridor',
    'staircase',
    'entrance',
    'level_1',
    'level_2',
    'level_3',
    'level_4',
    'level_5',
    'courtyard',
    'front_entrance'
  ];

  actions = [
    'door',
    'door_ahead',
    'door_then_right',
    'door_then_left',
    'door_right',
    'door_left',
    'forward',
    'left',
    'right',
    'up_level_1',
    'up_level_2',
    'up_level_3',
    'up_level_4',
    'up_level_5'
  ];
  nodeEditForm = this.formBuilder.group({
    label: ['', [Validators.required, Validators.minLength]],
    type: ['', [Validators.required]],
    action: ['', [Validators.required]]
  });

  constructor(private navNodeService: NavNodeService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
  }

  @Input('selectedId')
  set setSelectedId(id: number) {
    this.navNode = this.navNodeService.getNavNodeWithId(id);

    this.nodeEditForm.get('label').setValue(this.navNode.label);
    this.nodeEditForm.get('type').setValue(this.navNode.type);
    this.nodeEditForm.get('action').setValue(this.navNode.action);
  }

  onSubmit() {
    const update: UpdateData = {
      action: this.nodeEditForm.value.action,
      imageUrl: this.navNode.imageUrl,
      label: this.nodeEditForm.value.label,
      type: this.nodeEditForm.value.type
    };
    this.navNodeService.updateNode(this.navNode.id, update);
  }

  get label() {
    return this.nodeEditForm.get('label');
  }

  get type() {
    return this.nodeEditForm.get('type');
  }

  get action() {
    return this.nodeEditForm.get('action');
  }

  onImageUploadFinished() {
    console.log('image upload ready');
    this.isEditing = false;
  }

  onImageUploadStarted() {
    console.log('started');
  }

  onSwitchImageClick() {
    this.isEditing = true;
  }

  onCancelSwitchImage() {
    this.isEditing = false;
  }
}
