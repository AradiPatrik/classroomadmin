import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {Classroom, ClassroomService} from '../../classroom.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-classroom-list-component',
  templateUrl: './classroom-list-component.component.html',
  styleUrls: ['./classroom-list-component.component.css']
})
export class ClassroomListComponentComponent implements OnInit, OnChanges, OnDestroy {

  classrooms: Classroom[];
  private subscription: Subscription;
  @Input()
  navNodeId: number;

  constructor(private classroomService: ClassroomService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.subscription = this.classroomService
      .getClassroomsOfNavNode(this.navNodeId).subscribe((classrooms) => {
        this.classrooms = classrooms;
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.navNodeId.isFirstChange()) {
      this.subscription.unsubscribe();
      this.subscription = this.classroomService
        .getClassroomsOfNavNode(this.navNodeId).subscribe((classrooms) => {
          this.classrooms = classrooms;
        });
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  addClassroom() {
    this.classroomService.createEmptyClassroom(+this.activatedRoute.snapshot.paramMap.get('buildingId'), this.navNodeId);
  }
}
