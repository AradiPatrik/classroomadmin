import {Component, Input, OnInit} from '@angular/core';
import {Classroom, ClassroomService} from '../../classroom.service';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-classroom-list-item',
  templateUrl: './classroom-list-item.component.html',
  styleUrls: ['./classroom-list-item.component.css']
})
export class ClassroomListItemComponent implements OnInit {

  @Input()
  classroom: Classroom;
  classroomEditForm = this.formBuilder.group({
    name: ['', [Validators.required]],
    code: ['', [Validators.required]],
    position: ['', [Validators.required]],
    direction: ['', [Validators.required]]
  });
  directions = ['ahead', 'left', 'right'];

  constructor(private classroomService: ClassroomService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    console.log(this.classroom.id);
    this.classroomEditForm.get('name').setValue(this.classroom.name);
    this.classroomEditForm.get('code').setValue(this.classroom.code);
    this.classroomEditForm.get('position').setValue(this.classroom.position);
    this.classroomEditForm.get('direction').setValue(this.classroom.direction);
  }

  updateClassroom() {
    this.classroomService.updateClassroom(+this.classroom.id, this.classroomEditForm.value);
  }

  deleteClassroom() {
    this.classroomService.deleteClassroom(this.classroom.id);
  }

}
