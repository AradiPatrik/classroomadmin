import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NavNodeService} from '../nav-node.service';
import {BuildingGraphService} from '../building-graph.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-building-structure-editor',
  templateUrl: './building-structure-editor.component.html',
  styleUrls: ['./building-structure-editor.component.css']
})
export class BuildingStructureEditorComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  selectedNodeId: number;
  private buildingId: number;

  constructor(
    private activatedRoute: ActivatedRoute,
    private navNodeService: NavNodeService,
    private buildingGraphService: BuildingGraphService
  ) {
  }

  ngOnInit() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.buildingId = id;
    this.navNodeService.setBuilding(id);
    this.subscription = this.navNodeService.navNodesOfSelectedBuilding$.subscribe((navNodes) => {
      this.buildingGraphService.pushNavNodes(navNodes);
      console.log(this.buildingGraphService.graphNodes);
      console.log(this.buildingGraphService.graphEdges);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.buildingGraphService.clearNodes();
  }

  onNodeClick(id: number) {
    this.selectedNodeId = id;
  }

  onAddChildClick(id: number) {
    this.navNodeService.addEmptyChild(id, this.buildingId);
  }
}
