import {Injectable} from '@angular/core';
import {AngularFireAction, AngularFireDatabase, SnapshotAction} from '@angular/fire/database';
import {first, map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {BuildingGraphService} from './building-graph.service';
import {BuildingService} from './building.service';
import {UniqueIdService} from '../unique-id.service';

export interface NavNode {
  id: number;
  parentId: number;
  buildingId: number;
  label: string;
  action: string;
  type: string;
  imageUrl: string;
}

export interface UpdateData {
  label: string;
  action: string;
  type: string;
  imageUrl: string;
}

const missingImageUrl =
  'https://firebasestorage.googleapis.com/v0/b/szteclassroomfinder.appspot.com/o/' +
  'nav_node_pictures%2Fhallway.jpg?alt=media&token=e27564d5-0623-4e3d-8e21-122d7c777ae2';

function navNodeFromSnapshot(navNodeSnapshot: SnapshotAction<any>): NavNode {
  return {
    id: +navNodeSnapshot.key,
    parentId: navNodeSnapshot.payload.child('parentId').val(),
    buildingId: navNodeSnapshot.payload.child('buildingId').val(),
    label: navNodeSnapshot.payload.child('label').val(),
    action: navNodeSnapshot.payload.child('action').val(),
    type: navNodeSnapshot.payload.child('type').val(),
    imageUrl: navNodeSnapshot.payload.hasChild('imageUrl') ? navNodeSnapshot.payload.child('imageUrl').val() : missingImageUrl
  };
}

@Injectable({
  providedIn: 'root'
})
export class NavNodeService {

  navNodesOfSelectedBuilding: NavNode[];
  navNodesOfSelectedBuilding$: Observable<NavNode[]>;

  constructor(private db: AngularFireDatabase,
              private uniqueIdService: UniqueIdService) {

  }

  setBuilding(buildingId: number) {
    this.navNodesOfSelectedBuilding$ = this.db
      .list('/navNodes', ref => ref.orderByChild('buildingId').equalTo(buildingId))
      .snapshotChanges().pipe(
        map(navNodeSnapshots => this.mapToNavNodes(navNodeSnapshots)),
        tap(navNodes => {
          console.log(navNodes);
          this.navNodesOfSelectedBuilding = navNodes;
        })
      );
  }

  getNavNodeWithId(id: number): NavNode {
    return this.navNodesOfSelectedBuilding.find((navNode) => navNode.id === id);
  }

  addClassroomToNode(nodeId: number) {
  }

  addEmptyChild(nodeId: number, buildingId) {
    this.uniqueIdService.getUniqueIdOf('/navNodes')
      .subscribe((uniqueId) => {
        this.db.object(`navNodes/${uniqueId}`).set({
          parentId: nodeId,
          buildingId,
          label: 'please set label',
          action: '',
          type: ''
        });
      });
  }

  deleteNode(nodeId: number) {
    this.db.object(`/navNodes/${nodeId}`).remove().then(() => {
      console.log('remove successful');
    }, (reason) => {
      console.log('remove was not successful');
      console.log(reason);
    });
  }

  updateNode(nodeId: number, newValue: UpdateData) {
    this.db.object(`/navNodes/${nodeId}/`).update(newValue).then(
      () => {
        console.log(`successfully updated node ${nodeId}`);
      },
      (reason) => {
        console.log(reason);
      });
  }

  private mapToNavNodes(navNodeSnapshots): NavNode[] {
    return navNodeSnapshots.map(snapshot => navNodeFromSnapshot(snapshot));
  }
}
