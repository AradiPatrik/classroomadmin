import {Injectable} from '@angular/core';
import * as dagre from 'dagre';
import {Subject} from 'rxjs';

interface GraphNode {
  id: string;
  label: string;
  x: number;
  y: number;
}

interface Point {
  x: number;
  y: number;
}

interface Edge {
  id: string;
  beg: Point;
  mid: Point;
  end: Point;
}

@Injectable({
  providedIn: 'root'
})
export class BuildingGraphService {

  private graph = new dagre.graphlib.Graph({directed: true});
  private graphReadySource = new Subject();

  graphReady$ = this.graphReadySource.asObservable();
  graphNodes: GraphNode[] = [];
  graphEdges: Edge[] = [];
  width = 0;
  height = 0;

  constructor() {
    this.graph.setGraph({});
    this.graph.setDefaultEdgeLabel(() => ({}));
  }

  clearNodes() {
    this.graph.setGraph({});
    this.graphNodes = [];
    this.graphEdges = [];
  }

  pushNavNodes(navNodes) {
    for (const navNode of navNodes) {
      this.add(navNode);
    }
    this.graphReadySource.next();
    console.log(this.graphNodes);
  }

  add(navNode) {
    let isAlreadyInGraphNodes = false;
    for (const graphNode of this.graphNodes) {
      if (+graphNode.id === navNode.id) {
        isAlreadyInGraphNodes = true;
        break;
      }
    }

    if (!isAlreadyInGraphNodes) {
      this.graph.setNode(`${navNode.id}`, {label: navNode.label, navNode, width: 20, height: 20});
      if (navNode.parentId !== 0) {
        this.graph.setEdge(
          `${navNode.parentId}`,
          `${navNode.id}`, {asd: 'valami'});
      }
      dagre.layout(this.graph);
    } else {
      const n = this.graphNodes.find((graphNode) => +graphNode.id === navNode.id);
      n.label = navNode.label;
    }

    console.log('node ids');
    this.graph.nodes().forEach((nodeName) => {
      console.log(nodeName);
      const graphNodeToUpdate = this.graphNodes.find((graphNode) => graphNode.id === nodeName);
      const nodeInGraph = this.graph.node(nodeName);
      if (graphNodeToUpdate !== undefined) {
        graphNodeToUpdate.x = nodeInGraph.x;
        graphNodeToUpdate.y = nodeInGraph.y;
      } else {
        this.graphNodes.push({id: nodeName, label: nodeInGraph.label, x: nodeInGraph.x, y: nodeInGraph.y});
      }
    });
    this.graph.edges().forEach((edgeObj) => {
      console.log(edgeObj);
      const edgeInGraph = this.graph.edge(edgeObj);
      const graphEdgeToUpdate = this.graphEdges.find((graphEdge) => graphEdge.id === `${edgeObj.v}__${edgeObj.w}`);
      if (graphEdgeToUpdate !== undefined) {
        graphEdgeToUpdate.beg = edgeInGraph.points[0];
        graphEdgeToUpdate.mid = edgeInGraph.points[1];
        graphEdgeToUpdate.end = edgeInGraph.points[2];
      } else {
        this.graphEdges.push({
          id: `${edgeObj.v}__${edgeObj.w}`,
          beg: edgeInGraph.points[0],
          mid: edgeInGraph.points[1],
          end: edgeInGraph.points[2]
        });
      }
    });

    this.width = this.graph.graph().width;
    this.height = this.graph.graph().height;
  }

  updateLayout() {
    dagre.layout(this.graph);
    this.graph.nodes().forEach((nodeName) => {
      console.log(nodeName);
      const graphNodeToUpdate = this.graphNodes.find((graphNode) => graphNode.id === nodeName);
      const nodeInGraph = this.graph.node(nodeName);
      if (graphNodeToUpdate !== undefined) {
        graphNodeToUpdate.x = nodeInGraph.x;
        graphNodeToUpdate.y = nodeInGraph.y;
      } else {
        this.graphNodes.push({id: nodeName, label: nodeInGraph.label, x: nodeInGraph.x, y: nodeInGraph.y});
      }
    });
    this.graph.edges().forEach((edgeObj) => {
      console.log(edgeObj);
      const edgeInGraph = this.graph.edge(edgeObj);
      const graphEdgeToUpdate = this.graphEdges.find((graphEdge) => graphEdge.id === `${edgeObj.v}__${edgeObj.w}`);
      if (graphEdgeToUpdate !== undefined) {
        graphEdgeToUpdate.beg = edgeInGraph.points[0];
        graphEdgeToUpdate.mid = edgeInGraph.points[1];
        graphEdgeToUpdate.end = edgeInGraph.points[2];
      } else {
        this.graphEdges.push({
          id: `${edgeObj.v}__${edgeObj.w}`,
          beg: edgeInGraph.points[0],
          mid: edgeInGraph.points[1],
          end: edgeInGraph.points[2]
        });
      }
    });

    this.width = this.graph.graph().width;
    this.height = this.graph.graph().height;
  }

  deleteNode(nodeId: number) {
    const matched = this.graphNodes.filter((graphNode) => {
      return +graphNode.id === nodeId;
    })[0];

    if (matched !== undefined) {
      this.graphNodes.splice(this.graphNodes.indexOf(matched), 1);
    }

    this.graph.removeNode(`${nodeId}`);
    this.graphEdges.splice(this.graphEdges.indexOf(this.graphEdges.filter((edge) => {
      const graphEdgeEndId = edge.id.split('__')[1];
      return +graphEdgeEndId === nodeId;
    })[0]), 1);

    this.updateLayout();
  }

  isLeaf(nodeId: number) {
    for (const id of this.graph.nodes()) {
      const currentNode = this.graph.node(id);
      if (currentNode.navNode.parentId === nodeId) {
        return false;
      }
    }
    return true;
  }

  edgeToString(edge: { beg, mid, end }) {
    return `M ${edge.beg.x} ${edge.beg.y} q ${edge.mid.x - edge.beg.x}` +
      ` ${edge.mid.y - edge.beg.y} ${edge.end.x - edge.beg.x} ${edge.end.y - edge.beg.y}`;
  }
}
