import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DashboardComponent} from './dashboard/dashboard.component';
import {AdminRoutingModule} from './admin-routing.module';
import {
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {BuildingsComponent} from './buildings/buildings.component';
import {AdminsComponent} from './admins/admins.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {BuildingCardComponent} from './buildings/building-card/building-card.component';
import {BuildingStructureEditorComponent} from './buildings/building-structure-editor/building-structure-editor.component';
import {GraphDisplayComponent} from './buildings/building-structure-editor/graph-display/graph-display.component';
import {NodeEditComponent} from './buildings/building-structure-editor/node-edit/node-edit.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ClassroomListComponentComponent} from './buildings/building-structure-editor/classroom-list-component/classroom-list-component.component';
import {ClassroomListItemComponent} from './buildings/building-structure-editor/classroom-list-item/classroom-list-item.component';
import {LecturersComponent} from './teachers/lecturers/lecturers.component';
import {LecturerListComponent} from './teachers/lecturers/lecturer-list/lecturer-list.component';
import {LecturerListItemComponent} from './teachers/lecturers/lecturer-list-item/lecturer-list-item.component';
import { ImageUploadComponent } from './buildings/building-structure-editor/node-edit/image-upload/image-upload.component';
import {AngularFireStorageModule} from '@angular/fire/storage';

@NgModule({
  imports: [
    CommonModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AdminRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatListModule,
    MatCardModule,
    MatIconModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatSortModule,
    MatInputModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatTableModule,
    MatSelectModule
  ],
  declarations: [
    DashboardComponent,
    BuildingsComponent,
    AdminsComponent,
    BuildingCardComponent,
    BuildingStructureEditorComponent,
    GraphDisplayComponent,
    NodeEditComponent,
    ClassroomListComponentComponent,
    ClassroomListItemComponent,
    LecturersComponent,
    LecturerListComponent,
    LecturerListItemComponent,
    ImageUploadComponent
  ]
})
export class AdminModule {
}
