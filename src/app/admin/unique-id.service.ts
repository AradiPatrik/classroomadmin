import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {Observable} from 'rxjs';
import {first} from 'rxjs/internal/operators/first';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UniqueIdService {

  constructor(private db: AngularFireDatabase) {
  }


  private getUniqueId(ids: number[]): number {
    let uniqueId = 1;
    while (true) {
      const foundId = ids.filter((id) => id === uniqueId);
      if (foundId.length === 0) {
        return uniqueId;
      }
      uniqueId++;
    }
  }

  getUniqueIdOf(path: string): Observable<number> {
    return this.db.list(path).snapshotChanges().pipe(
      first(),
      map((snapshots) => {
        return snapshots.map(snapshot => +snapshot.key);
      }),
      map(this.getUniqueId)
    );
  }
}
