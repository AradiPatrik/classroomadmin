import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {BuildingsComponent} from './buildings/buildings.component';
import {AdminsComponent} from './admins/admins.component';
import {BuildingStructureEditorComponent} from './buildings/building-structure-editor/building-structure-editor.component';
import {LecturersComponent} from './teachers/lecturers/lecturers.component';
import {AuthGuard} from '../auth/auth.guard';

const adminRoutes: Routes = [
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], children: [
      {path: 'buildings', component: BuildingsComponent},
      {path: 'admins', component: AdminsComponent},
      {path: 'buildings/:id', component: BuildingStructureEditorComponent},
      {path: 'lecturers', component: LecturersComponent}
    ]}
];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule {
}
