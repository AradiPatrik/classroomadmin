import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {Observable} from 'rxjs';
import {first, map, tap} from 'rxjs/operators';
import {UniqueIdService} from '../unique-id.service';
import {LecturerUpdate} from './lecturers/lecturer-list-item/lecturer-list-item.component';

export interface Lecturer {
  id: number;
  code: string;
  email: string;
  name: string;
  officeId: number;
}

function mapToLecturer(lecturerSnapshot): Lecturer[] {
  return lecturerSnapshot.map(snapshot => ({
    id: +snapshot.key,
    code: snapshot.payload.child('code').val(),
    email: snapshot.payload.child('email').val(),
    name: snapshot.payload.child('name').val(),
    officeId: snapshot.payload.child('officeId').val()
  }));
}

@Injectable({
  providedIn: 'root'
})
export class LecturerService {

  constructor(private db: AngularFireDatabase,
              private uniqueIdService: UniqueIdService) {
  }

  getLecturers(): Observable<Lecturer[]> {
    return this.db
      .list('/lecturer')
      .snapshotChanges().pipe(
        map(lecturerSnapshot => mapToLecturer(lecturerSnapshot)),
        tap(lecturer => {
          console.log(lecturer);
        })
      );
  }

  updateLecturer(id: number, lecturerUpdate: LecturerUpdate) {
    this.db.object(`/lecturer/${id}/`).update(lecturerUpdate).then(
      () => {
        console.log(`successfully updated node ${id}`);
      },
      (reason) => {
        console.log(reason);
      });
  }


  addEmptyLecturer() {
    this.uniqueIdService.getUniqueIdOf('/lecturer').subscribe((uniqueId) => {
      this.db.object(`lecturer/${uniqueId}`).set({
        code: 'code',
        email: 'email',
        name: 'name',
        officeId: 'officeId'
      });
    });
  }

  deleteLecturer(lecturerId: number) {
    this.db.object(`/lecturer/${lecturerId}`).remove().then(() => {
      console.log('lecturer deleted successfully');
    }, (reason) => {
      console.log('lecturer could not be deleted');
      console.log(reason);
    });
  }


}
