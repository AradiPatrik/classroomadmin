import {Component, Input, OnInit} from '@angular/core';
import {Lecturer, LecturerService} from '../../lecturer.service';
import {FormBuilder, Validators} from '@angular/forms';

export interface LecturerUpdate {
  name: string;
  code: string;
  email: string;
  officeId: string;
}

@Component({
  selector: 'app-lecturer-list-item',
  templateUrl: './lecturer-list-item.component.html',
  styleUrls: ['./lecturer-list-item.component.css']
})
export class LecturerListItemComponent implements OnInit {

  @Input()
  lecturer: Lecturer;
  lecturerEditForm = this.formBuilder.group(
    {
      name: ['', [Validators.required]],
      code: ['', [Validators.required]],
      email: ['', [Validators.required]],
      officeId: ['', [Validators.required]]
    }
  );

  constructor(private lecturerService: LecturerService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    console.log(`Init: lecturer: ${this.lecturer.id} with value ${this.lecturer}`);
    this.lecturerEditForm.get('name').setValue(this.lecturer.name);
    this.lecturerEditForm.get('code').setValue(this.lecturer.code);
    this.lecturerEditForm.get('email').setValue(this.lecturer.email);
    this.lecturerEditForm.get('officeId').setValue(this.lecturer.officeId);
  }


  updateLecturer() {
    this.lecturerService.updateLecturer(this.lecturer.id, this.lecturerEditForm.value);
  }

  deleteLecturer() {
    this.lecturerService.deleteLecturer(this.lecturer.id);
  }
}
