import {Component, OnDestroy, OnInit} from '@angular/core';
import {Lecturer, LecturerService} from '../../lecturer.service';
import {Subscription} from 'rxjs';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-lecturer-list',
  templateUrl: './lecturer-list.component.html',
  styleUrls: ['./lecturer-list.component.css']
})
export class LecturerListComponent implements OnInit, OnDestroy {

  loaded = false;
  lecturers: Lecturer[];
  lecturersFullList: Lecturer[];
  lecturersSubscription: Subscription;
  searchSubscription: Subscription;

  search = new FormControl('');


  constructor(private lecturerService: LecturerService) {
  }

  ngOnInit() {
    this.lecturersSubscription = this.lecturerService.getLecturers().subscribe((val) => {
      this.loaded = true;
      this.lecturers = val;
      this.lecturersFullList = val;
    });

    this.searchSubscription = this.search.valueChanges.subscribe(
      (val) => this.lecturers = this.lecturersFullList.filter(lecturer => lecturer.name.match(new RegExp(val, 'i'))));
  }

  addLecturer() {
    console.log('add lecturer');
    this.lecturerService.addEmptyLecturer();
  }

  ngOnDestroy(): void {
    this.searchSubscription.unsubscribe();
    this.lecturersSubscription.unsubscribe();
  }

}
